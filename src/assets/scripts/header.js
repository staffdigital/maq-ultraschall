$(function(){
	//clone
	$('.header_cuenta_box2').clone().appendTo('.menu__mobile__login').removeClass('header_cuenta_box2').removeClass('hide').addClass('menu_mobile_login_box');

	// +++ efecto hover de productos +++
	$('.header .header_menu_item.hover').hover(
		function(){
			$('.header_menu_overlay').addClass('active');
			$('body').addClass('active');
		},
		function(){
			$('.header_menu_overlay').removeClass('active');
			$('body').removeClass('active');
		}
	);
	// <<< end >>>

	// menu aside - plantilla
		$('.btn__mobile').click(function() {
			$('.header_menu_mobile_overlay').addClass('active');
			$('body').addClass('active');
			$('.menu__mobile__prin').addClass('active');
		});
		$('.btn__menu__close__mobile').click(function() {
			$('.header_menu_mobile_overlay').removeClass('active');
			$('body').removeClass('active');
			$('.mega__menu__mobile').removeClass('active');
		});
		$('.btn__cat__mobile').click(function() {
			event.preventDefault();
			$('.menu__mobile__prin').removeClass('active');
			$('.menu__mobile__categorias__prin').addClass('active');
			var bdataid = $(this).attr('data-id');
			$('.menu__mobile__categorias__lists ul').removeClass('active');
			$('.menu__mobile__categorias__lists ul[id="'+bdataid+'"]').addClass('active');
		});
		$('.categorias__title__mobile').click(function() {
			$('.menu__mobile__prin').addClass('active');
			$('.menu__mobile__categorias__prin').removeClass('active');
		});
		$('.btn__subcat__mobile').click(function() {
			event.preventDefault();
			$('.menu__mobile__categorias__prin').removeClass('active');
			$('.menu__mobile__subcategorias').addClass('active');
			var bdataid = $(this).attr('data-id');
			$('.menu__mobile__subcategorias__lists ul').removeClass('active');
			$('.menu__mobile__subcategorias__lists ul[id="'+bdataid+'"]').addClass('active');
		});
		$('.subcategorias__title__mobile').click(function() {
			$('.menu__mobile__categorias__prin').addClass('active');
			$('.menu__mobile__subcategorias').removeClass('active');
		});

		$('.header_menu_mobile_overlay').click(function() {
			$(this).removeClass('active');
			$('body').removeClass('active');
			$('.mega__menu__mobile').removeClass('active');
		});
	// <<< end >>>

	//detectando tablet, celular o ipad
	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};
	
	// // dispositivo_movil = $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
	// if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	// 	// tasks to do if it is a Mobile Device
	// 	function readDeviceOrientation() {
	// 		if (Math.abs(window.orientation) === 90) {
	// 			// Landscape
	// 			cerrar_nav();
	// 		} else {
	// 			// Portrait
	// 			cerrar_nav();
	// 		}
	// 	}
	// 	window.onorientationchange = readDeviceOrientation;
	// }else{
	// 	$(window).resize(function() {
	// 		var estadomenu = $('.menu-responsive').width();
	// 		if(estadomenu != 0){
	// 			cerrar_nav();
	// 		}
	// 	});
	// }
	

	// header scroll
	var altoScroll = 0
	$(window).scroll(function() {
		altoScroll = $(window).scrollTop();
		if (altoScroll > 0) {
			$('.header-fixed').addClass('scrolling');
		}else{
			$('.header-fixed').removeClass('scrolling');
		};
	});
});

$(function(){
	// +++ MI CUENTA +++
	$('.header_cuenta_link').click(function(event) {
		event.preventDefault();
		$(this).toggleClass('active');
		$('.header_cuenta_boxes').slideToggle();
	});
	// <<< end >>>

	// +++ SEARCH ITEMS +++
	$( ".header_search_input input" ).on( "keypress", function() {
		$('.header_search_block').addClass('active');
		$('.header_search_overlay').addClass('active');
		$('body').addClass('active');
	});	
	// <<< end >>>

	// +++ SEARCH OVERLAY +++
	$('.header_search_overlay').click(function(event) {
		$('.header_search_block').removeClass('active');
		$('.header_search_overlay').removeClass('active');
		$('body').removeClass('active');
	});
	// <<< end >>>
	
	// MENU MOBILE LINKS EXTERNOS
	$('.menuOpenLinksToggle').click(function(event) {
		event.preventDefault();
		$('.menu_link_mobile_toggle').toggleClass('active_toggle');
		$('.menu_links_mobile_toggle').slideToggle('fast');
	});
	// <<< end >>>
});