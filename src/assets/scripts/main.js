$(() => {
	// +++ CARRITO DE COTIZACIONES +++
	$('.btn__car__prin').click(function() {
		 event.preventDefault();
		$('.ctn__car__sidebar').addClass('active');
		$('.header_car_overlay').addClass('active');
		$('body').addClass('active');
	});

	$('.btn__car__close, .header_car_overlay, .car__vacio__close').click(function() {
		$('.ctn__car__sidebar').removeClass('active');
		$('.header_car_overlay').removeClass('active');
		$('body').removeClass('active');
	});

	// delet item
	$('.item__car__delete').click(function() {
		$(this).closest('.item__car').fadeOut('fast');
	});
	// <<< end >>>

	// cantidad en input sin numeros en negativos, inicial 1
	function updateCantidad(obj, operacion) {
		var padre = obj.parent();
		var input = padre.find('input');
		var cant = input.val() < 1 ? 1 : parseInt(input.val());
	if( operacion == '-' ){
		cant = cant - 1;
		cant = cant < 1 ? 1 : cant;
	}else{
		cant = cant + 1;
	}
		input.val(cant).trigger('change');
	}

	$('.button__more').click(function(){
		updateCantidad($(this), '+');
	});

	$('.button__minus').click(function(){
		updateCantidad($(this), '-');
	});

	// inputs solo numeros
	$('.soloNumber').each((k, el)=>{
		$(el).on('keydown', function(evt){
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
			return true;
		});
	});

	$('.input-one').focusin((e) => {
		$(e.target).parent('.input-one-line').addClass('active')
	})
	.focusout((e) => {
		if(!e.target.value.length) {
			$(e.target).parent('.input-one-line').removeClass('active')
		}
	})
	$('.input-one').keyup((e) => {
		if(e.target.value.length) {
			$(e.target).parent('.input-one-line').addClass('active')
		}else{
			$(e.target).parent('.input-one-line').removeClass('active')
		}
	})

	// animation--banner general
	$('.banner__general').each(function(index, el) {
		$(el).waypoint(function(direction) {
			if (direction === 'down') {
				$(el).addClass('active');
			}else {
				$(el).removeClass('active');
			}
		}, {
			offset:'100%'
		});
	});
})